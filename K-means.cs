using System;
using System.Collections.Generic;

///
/// Seyedamir Makinejadsanij
/// K-Means Algorith With Euclidean Distance
///
namespace IASC
{
    internal class Kmeans
    {
        public class Point
        {
            public int Id { get; set; }

            public int X { get; set; }
        }

        public class PointCollection : List<Point>
        {
            public double Centroid;

            public void CalculateCentroid()
            {
                double sum = 0;
                int counter = 0;
                foreach (Point clusterPoint in this)
                {
                    sum += clusterPoint.X;
                    counter++;
                }
                this.Centroid = 1.0 * sum / counter;
            }

            internal Point RemovePoint(Point point)
            {
                try
                {
                    this.Remove(point);
                    this.Centroid = 1.0 * ((this.Count * this.Centroid) - point.X) / (this.Count - 1);
                }
                catch (Exception ex)
                {
                    Logger.Log(ex.Message, LogCategory.ManagedError);
                }
                return point;
            }

            internal void AddPoint(Point removedPoint)
            {
                try
                {
                    this.Add(removedPoint);
                    this.Centroid = 1.0 * ((this.Count - 1) * this.Centroid + removedPoint.X) / (this.Count);
                }
                catch(Exception ex)
                {
                    Logger.Log(ex.Message, LogCategory.ManagedError);
                }
            }
        }

        public static List<PointCollection> DoKMeans(PointCollection points, int clusterCount)
        {
            List<PointCollection> allClusters = new List<PointCollection>();
            List<List<Point>> allGroups = SplitList(points, clusterCount);
            try
            {
                foreach (List<Point> group in allGroups)
                {
                    PointCollection cluster = new PointCollection();

                    cluster.AddRange(group);
                    cluster.CalculateCentroid();
                    allClusters.Add(cluster);
                }

                //start k-means clustering
                int movements = 1;
                int ThressholdCounter = 1;
                while (movements > 0 && ThressholdCounter < 1000)
                {
                    movements = 0;
                    ThressholdCounter++;
                    foreach (PointCollection cluster in allClusters) //for all clusters
                    {
                        for (int pointIndex = 0; pointIndex < cluster.Count; pointIndex++) //for all points in each cluster
                        {
                            Point point = cluster[pointIndex];

                            int nearestCluster = FindNearestCluster(allClusters, point);
                            if (nearestCluster != allClusters.IndexOf(cluster)) //if point has moved
                            {
                                if (cluster.Count > 1) //each cluster shall have minimum one point
                                {
                                    Point removedPoint = cluster.RemovePoint(point);
                                    allClusters[nearestCluster].AddPoint(removedPoint);
                                    movements += 1;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message, LogCategory.ManagedError);
            }
            return allClusters;
        }

        private static int FindNearestCluster(List<PointCollection> allClusters, Point point)
        {
            double clusterMean = double.MaxValue;
            int clusterId = 0;
            int clusterCounetr = 0;
            foreach (PointCollection cluster in allClusters)
            {
                if (clusterMean > Math.Abs(point.X - cluster.Centroid))
                {
                    clusterMean = Math.Abs(point.X - cluster.Centroid);
                    clusterId = allClusters.IndexOf(cluster);
                }
                clusterCounetr++;
            }
            return clusterId;
        }

        private static List<List<Point>> SplitList(PointCollection points, int clusterCount)
        {
            int reminder = 0;
            int BatchSize = Math.DivRem(points.Count, clusterCount, out reminder);
            if (reminder > 0)
                BatchSize++;
            List<List<Point>> result = new List<List<Point>>();
            PointCollection[] OneClusterData = new PointCollection[clusterCount];
            int counter = 0;
            int pointCounter = 0;
            OneClusterData[counter] = new PointCollection();
            foreach (Point onePoint in points)
            {
                OneClusterData[counter].AddPoint(onePoint);
                pointCounter++;
                if (pointCounter >= BatchSize)
                {
                    result.Add(OneClusterData[counter]);
                    counter++;
                    if (counter == clusterCount)
                        return result;
                    OneClusterData[counter] = new PointCollection();
                    pointCounter = 0;
                }
            }
            result.Add(OneClusterData[counter]);
            return result;
        }
    }
}